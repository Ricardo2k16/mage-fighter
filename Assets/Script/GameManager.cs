﻿using UnityEngine;
using System.Collections;

public class GameManager : MonoBehaviour {

    public GameObject player1;
    public GameObject player2;

    public GameObject keyboard;

    public System.Collections.Generic.List<char> keysP1;
    public System.Collections.Generic.List<char> keysP2;

    System.Collections.Generic.List<char> keyboardAtivo;

    public KeyCode resetGame;

    public GameObject keyboardController;

    public BattleText textUI;

    public float originalTimeToPress;
    public float minimumTimeToPress;
    public float decreaseInterval;

    public int playerInicial;
    float currentTimeToPress;

    string sequence;

    int currentPlayer;
    int currentButton = 0;
    int turn;
    public int sumDamage;

    bool shouldGenerateSequence = false;

    int currentRound = 1;
    float timeToPress;

    public Animator animControllerP1;
    public Animator animControllerP2;

    // Use this for initialization
    void Start() {
        keyboardAtivo = keysP2;

        //No início o game manager deve gerar uma sequencia
        generateSequence(keyboardAtivo.Count);
        //Inicializar o timer com o tempo que o jogador tem para pressionar CADA tecla
        currentTimeToPress = originalTimeToPress;
        //Define de qual jogador é o turno
        currentPlayer = playerInicial;
        //Exibe a sequencia; outra interface deve ser resolvida depois
        Debug.Log(sequence);
        timeToPress = originalTimeToPress;
        textUI.startRound();
    }

    // Update is called once per frame
    void Update() {
        if (roundIsEnded()) {
            //Ricardo se vira
            //Verifica se ha um vencedor

            if( roundIsEnded() )
            {
                

                if ( player1.GetComponent<Health>().isAlive() )
                {
                    // p1 ganhou!
                    player1.GetComponent<Player>().win++;
                    Debug.Log("p1!");
                }
                else
                {
                    //p2 ganhou!
                    player2.GetComponent<Player>().win++;
                    Debug.Log("p2!");
                }

                if( hasWinner() )
                {
                    textUI.endBattle();
                    //FIM DE JOGO!
                    Debug.Log("FIM DE JOGO!");
                    if( player1.GetComponent<Player>().win >= 2)
                    {
                        Debug.Log("P1 WINS!");
                    }
                    else
                    {
                        Debug.Log("P2 WINS!");
                    }
                }
                else
                {
                    //Se o jogo não acabou, passa o round
                    currentRound++;
                    originalTimeToPress = timeToPress;
                    //Reseta a vida dos dois jogadores
                    player1.GetComponent<Health>().ressurect();
                    player2.GetComponent<Health>().ressurect();
                    textUI.startRound();
                    
                }

            }
            return;
        }
        //Verifica de quem é o turno
        //Verifica se está na hora de apertar o botao
        ////print(correctButtonIsDown('q'));
        //if (sequenceHasEnded()) {
        //    dealDamage(currentPlayer, sumDamage);
        //    currentButton = 0;
        //    nextPlayer();
        //    this.generateSequence(keys.Count);
        //}
        //else
        //{
        //    //Se não existe vencedor, uma nova sequencia deve ser gerada
        //    
        //    //Debug.Log(this.sequence);
        //}


        if (Time.fixedDeltaTime < currentTimeToPress)
        {

            if (sequenceHasEnded())
            {
                //Se a sequencia inteira foi pressionada, exibe a mensagem de acerto
                Debug.Log("Sequencia inteira certa!");
                //Dá dano no outro jogador
                if (currentPlayer == 2)
                    dealDamage(2, sumDamage * 2);
                else
                    dealDamage(1, sumDamage * 2);
                //Passa o turno
                passTurn();
            }
            else if (Input.anyKeyDown) // Esta função tem uma entrada temperamental do teclado; rever para ver se tem alguma coisa que possa ser feita. Pedir ajuda para o cada do Unity?
            {
                
                //if( correctButtonIsDown( Input.inputString.ToCharArray()[0]) ) <- Não sei por que não funcionou; é tão linda esta função!
                if ( sequence.ToCharArray()[currentButton] == Input.inputString.ToCharArray()[0] ) //Se a linha de cima funcionar algum dia, remover esta linha
                {
                    //O botão certo foi pressionado; espera o próximo
                    keyboardController.GetComponent<tecladoController>().buttons[keyboardAtivo.FindIndex(x => x == sequence.ToCharArray()[currentButton])].GetComponent<ParticleSystem>().Emit(100);
                    //Reinicia o contador; o contador é para pressionar CADA tecla ou TODAS as teclas?
                    //Este código considera como se fosse para CADA tecla, por isso tem que reiniciar o contador!
                    currentTimeToPress = originalTimeToPress;
                    currentButton++;
                    //Exibe a mensagem para o jogador ver que a tecla certa foi pressionada
                    Debug.Log("Botão correto, esperando o caracter " + currentButton + " da sequencia!");
                }
                else
                {
                    //Se a tecla incorreta foi pressionada, dá a mensagem de erro
                    Debug.Log("Botão incorreto, esperado o botão " + sequence.ToCharArray()[currentButton] +
                        ", " + currentButton + " da sequencia, recebido " + Input.inputString.ToCharArray()[0]);
                    //Dá dano no jogador atual pela falha da invocação
                    //dealDamage(currentPlayer, sumDamage);
                    if (currentPlayer == 1)
                        dealDamage(2, sumDamage);
                    else
                        dealDamage(1, sumDamage);
                    //Passa o turno
                    passTurn();
                }

                
            }
            else
            {
                //Conta o tempo enquanto o jogador não pressiona tecla nenhuma
                currentTimeToPress -= Time.fixedDeltaTime;
                keyboardController.GetComponent<tecladoController>().setButtonToPress(
                    keyboardAtivo.FindIndex(x => x == sequence.ToCharArray()[currentButton]));
            }

            

        }
        else
        {
            Debug.Log("Time UP!");
            //Se o jogador não apertou a tempo a tecla,
            //Dá dano
            if (currentPlayer == 1)
                dealDamage(2, sumDamage);
            else
                dealDamage(1, sumDamage);
            //dealDamage(currentPlayer, sumDamage); //Dano de 10 fixo? Como calcular esse número?
            //Debug.Log(sumDamage + " de dano no player " + currentPlayer);
            //Passa o turno
            passTurn();
        }

        
    }

    void passTurn()
    {
        if( originalTimeToPress > minimumTimeToPress )
        {
            originalTimeToPress -= decreaseInterval;
        }
        Debug.Log("Novo tempo para apertar" + originalTimeToPress);
        //Reinicia o timer para a próxima sequencia
        currentTimeToPress = originalTimeToPress;

        if (currentPlayer == 1)
        {
            animControllerP1.SetBool("shouldCast", true);
            animControllerP2.SetBool("shouldCast", false);
            keyboardAtivo = keysP1;
        }
        else
        {
            animControllerP1.SetBool("shouldCast", false);
            animControllerP2.SetBool("shouldCast", true);
            keyboardAtivo = keysP2;
        }

        //Passa o turno para o próximo player
        nextPlayer();
        currentButton = 0;
        
        //player2.GetComponent<Animator>().

        this.generateSequence(keyboardAtivo.Count);
        Debug.Log(sequence);
    }

    void generateSequence(int length)
    {
        string newSequence = "";
        int nKeys = keyboardAtivo.Count;
        for (int i = 0; i < length; i++)
        {
            newSequence += keyboardAtivo[Random.Range(0, nKeys)];
        }
        sequence = newSequence;
    }

    char getNextKey()
    {
        return keyboardAtivo[Random.Range(0, keyboardAtivo.Count)];
    }

    void dealDamage(int idPlayer, int damage)
    {
        if (idPlayer == 1)
        {
            player2.GetComponent<ParticleSystem>().Emit(100);
            player1.GetComponent<Health>().takeDamage(damage);
        }
        else if (idPlayer == 2)
        {
            player1.GetComponent<ParticleSystem>().Emit(100);
            player2.GetComponent<Health>().takeDamage(damage);
        }
    }

    bool hasWinner() {
        if (player1.GetComponent<Player>().win >= 2 || player2.GetComponent<Player>().win >= 2)
            return true;
        return false;
    }

    bool roundIsEnded()
    {
        if(!player1.GetComponent<Health>().isAlive() || !player2.GetComponent<Health>().isAlive())
            return true;
        return false;
    }

    bool correctButtonIsDown(char button)
    {
        string pressedButtons = "";
        foreach (char key in keyboardAtivo) {
            if (Input.GetKey(key.ToString()))
            {
                pressedButtons += key;
            }
        }
        if (button.ToString() == pressedButtons)
            return true;
        return false;
    }

    bool sequenceHasEnded() {

        return sequence.Length == currentButton;

        //int sequenceCounter = sequence.Length;
        //if (sequenceCounter == currentButton)
        //    return true;
        //return false;

    }

    void nextPlayer()
    {
        currentPlayer = (currentPlayer + 2) % 2 + 1;
        keyboard.GetComponent<tecladoController>().currentPlayer = currentPlayer;
    }

    public int getRound()
    {
        return currentRound;
    }

    public int getWinner()
    {
        if(player1.GetComponent<Player>().win > player2.GetComponent<Player>().win)
        {
            return 1;
        }
        return 2;
    }
}
