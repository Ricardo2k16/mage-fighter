﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class tecladoController : MonoBehaviour {

    public List<GameObject> buttons;
    public int currentPlayer;

    SpriteRenderer keyboardSprite;

    public Color p1Color;
    public Color p2Color;

	// Use this for initialization
	void Start () {
        keyboardSprite = GetComponent<SpriteRenderer>();
	}
	
	// Update is called once per frame
	void Update ()
    {
	
        if( currentPlayer == 1 )
        {
            keyboardSprite.color = p1Color;
            foreach( GameObject button in buttons )
            {
                button.GetComponent<SpriteRenderer>().color = p1Color;
                if ( !button.GetComponent<btnController>().toPress )
                {
                    Color updated = p1Color;
                    updated.a = 0.5f;
                    button.GetComponent<SpriteRenderer>().color = updated;
                }
                else
                {
                    Color updated = p1Color;
                    updated.a = 1.0f;
                    button.GetComponent<SpriteRenderer>().color = updated;
                }
            }
        }
        else
        {
            keyboardSprite.color = p2Color;
            foreach (GameObject button in buttons)
            {
                button.GetComponent<SpriteRenderer>().color = p2Color;
                if (!button.GetComponent<btnController>().toPress)
                {
                    Color updated = p2Color;
                    updated.a = 0.5f;
                    button.GetComponent<SpriteRenderer>().color = updated;
                }
                else
                {
                    Color updated = p2Color;
                    updated.a = 1.0f;
                    button.GetComponent<SpriteRenderer>().color = updated;
                }
                //Color updated = p2Color;
                //updated.a = 0.5f;
                //button.GetComponent<SpriteRenderer>().color = updated;
            }
        }

	}

    public void setButtonToPress(int index)
    {
        for(int i = 0; i < buttons.Count; i++ )
        {
            if( i == index )
                buttons[i].GetComponent<btnController>().toPress = true;
            else
                buttons[i].GetComponent<btnController>().toPress = false;
        }
        
    }

}
