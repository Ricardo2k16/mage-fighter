﻿using UnityEngine;
using UnityEngine.UI;

public class CanvasController : MonoBehaviour {

    public Slider healthBarPrefab;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public Slider createNewHealthBar(Vector3 position)
    {
        Slider healthBar = Instantiate(healthBarPrefab, position, Quaternion.identity) as Slider;
        healthBar.transform.SetParent(gameObject.transform);
        healthBar.transform.position = position;
        healthBar.transform.localScale = new Vector3(1, 1, 1);
        return healthBar;
    }

    public void destroySlider(Slider slider) { }
}
