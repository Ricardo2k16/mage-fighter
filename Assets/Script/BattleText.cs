﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class BattleText : MonoBehaviour {

    public GameManager gameManager;

    public Animator animController;
    [HideInInspector] public enum TextState { StartRound, Idle, EndBattle};
    string text = "Round 1";
	// Use this for initialization
	void Start () {
        animController = gameObject.GetComponent<Animator>();
	}
	
	// Update is called once per frame
	void Update () {
        //Verificar se a animação acabou se acabou vai pra idle
        //if(animController.)
	}

    public void startRound() {
        text = "Round " + gameManager.getRound().ToString();
        animController.SetInteger("TextState", (int) TextState.StartRound );
        GetComponent<Text>().text = text;
    }

    public void endBattle() {
        //Pega o vencedor e dependendo do vencedor dado pelo game manager altera o texto
        text = "Player " + gameManager.getWinner() + " wins";

        animController.SetInteger("TextState", (int)TextState.StartRound);
        GetComponent<Text>().text = text;
    }

    public void startIdle()
    {
        GetComponent<Text>().text = "";
    }
}
